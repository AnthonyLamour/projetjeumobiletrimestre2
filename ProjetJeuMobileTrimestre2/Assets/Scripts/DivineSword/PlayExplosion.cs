﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayExplosion : MonoBehaviour
{
    //volumeManagers correspond au script permettant de gérer le volume
    private VolumeManager volumeManager;

    //audioSource correspond à la source audio de l'objet
    private AudioSource audioSource;

    //ennemisSpwaner correspond à l'objet gérant le spawn des énnemis
    private SpawnEnnemis ennemisSpawner;

    //est appeller à l'activation de l'objet
    private void Start()
    {
        //initialisation de volumeManager
        while (volumeManager == null)
        {
            volumeManager = GameObject.FindGameObjectWithTag("Parameters").GetComponent<VolumeManager>();
        }

        //initialisaion de audioSource
        while (audioSource == null)
        {
            audioSource = this.GetComponent<AudioSource>();
        }

        //initialisation de ennemisSpawner
        while(ennemisSpawner == null)
        {
            ennemisSpawner = GameObject.FindGameObjectWithTag("EnnemisSpawner").GetComponent<SpawnEnnemis>();
        }

        //modification du volume du son
        audioSource.volume = volumeManager.GetSFXVolume();
    }

    //permet de démarrer l'explosion
    private void PlaySound()
    {
        //démarrage du son de l'explosion
        audioSource.Play();
        //arrêt du spawn des ennemis
        ennemisSpawner.StopSpawning();
    }

    //permet d'arrêter l'explosion
    private void StopExplosion()
    {
        //relancement du spawn des ennemis
        ennemisSpawner.SpawnBack();
        //désactivation du divineStrike
        this.gameObject.SetActive(false);
    }
}
