﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DivineSwordController : MonoBehaviour
{
    //divineStrile correspond à l'objet applicant le bonus de la divine sword
    public GameObject divineStrike;


    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;

    //speed est la vitesse de déplacement de l'épée
    private float speed;

    //isMoving sert à savoir si l'épée est en mouvement
    private bool isMoving;

    //swordRigidbody2D correspond au rigidbody de l'épée
    private Rigidbody2D swordRigidbody2D;

    //est appeller lors de l'activation de l'objet
    private void Start()
    {
        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }
        
        //initialisation de swordRigidbody2D
        while(swordRigidbody2D == null)
        {
            swordRigidbody2D = this.GetComponent<Rigidbody2D>();
        }

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //est appeller à chaque tick
    void Update()
    {

        //si l'épée est en mouvement
        if (isMoving)
        {
            //modification de la vélocité de l'épée
            swordRigidbody2D.velocity = new Vector2(0, speed);
        }
    }

    //permet de modifier le statue de l'épée
    public void SwitchIsMoving()
    {
        //si l'épée est en mouvement
        if (isMoving)
        {
            //modification de isMoving
            isMoving = false;
            //reset de la vitesse de l'épée
            speed = 0;
            //reset de la vélocité de l'épée
            swordRigidbody2D.velocity = new Vector2(0, 0);
        }
        //sinon
        else
        {
            //modification de isMoving
            isMoving = true;
        }
    }

    //sert à setter la vitesse de l'épée
    public void SetSpeed(float newSpeed)
    {
        //modification de la vitesse de l'épée
        speed = newSpeed;
    }

    //sert à reset l'épée
    public void ResetDivineSword()
    {
        //reset de la position de l'épée en dehors de l'écran
        this.transform.position = new Vector2(-2500, 1250);
        //appelle de SwitchIsMoving
        SwitchIsMoving();
    }

    //lorsque l'objet rentre en collision
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //si l'objet entrant en collision est le joueur
        if (collision.gameObject.tag == "Player")
        {
            //activation du divineStrike
            divineStrike.SetActive(true);
            //appelle de ResetDivineSword
            ResetDivineSword();
        }
    }

    //permet de récupérer le bool isMoving
    public bool GetIsMoving()
    {
        //renvoie de isMoving
        return isMoving;
    }
}
