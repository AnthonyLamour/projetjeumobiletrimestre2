﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    //sceneName correspond au nom de la scène à charger
    public string sceneName;

    //permet de charger la scène suivante
    public void LoadSceneByName()
    {
        //chargement de la scène en fonction du nom
        SceneManager.LoadScene(sceneName);
    }
}
