﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadIt : MonoBehaviour
{
    //loader correspond à l'objet gérant le chargement des objets de la scène
    private GameObject loader;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de loader
        while(loader == null)
        {
            loader = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //envoi du signale pour charger l'objet suivant
        loader.GetComponent<LoadingScript>().Notify();
    }
}
