﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScript : MonoBehaviour
{

    //objectsToLoad correspond à la liste des objets à charger
    public GameObject[] objectsToLoad;

    //readyPanel correspond au panel de préparation
    public GameObject readyPanel;

    //cpt est le compteur d'objet déjà charger
    private int cpt;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //activation du premier objet
        objectsToLoad[0].gameObject.SetActive(true);
        //initialisation du compteur
        cpt = 0;
    }

    //est appeller à la fin du chargement de chaque objet
    public void Notify()
    {
        //incrémentation du compteur
        cpt = cpt + 1;
        //si il reste des objets à charger
        if (cpt < objectsToLoad.Length)
        {
            //activation de l'objet suivant
            objectsToLoad[cpt].SetActive(true);
        }
        //sinon
        else
        {
            //affichage du panel de préparation
            readyPanel.SetActive(true);
            //destruction du panel de chargement
            Destroy(this.gameObject);
        }
    }
}
