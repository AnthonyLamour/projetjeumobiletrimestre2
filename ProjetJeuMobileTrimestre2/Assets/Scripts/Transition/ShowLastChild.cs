﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowLastChild : MonoBehaviour
{

    //est appeller à chaque activation de l'objet
    private void OnEnable()
    {
        //activation du dernier enfant de l'objet
        this.transform.GetChild(this.transform.childCount - 1).gameObject.SetActive(true);       
    }

    //permet d'afficher le game over d'épée briser
    public void BrokenSword()
    {
        //affichage de l'épée briser
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);
    }
}
