﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionScript : MonoBehaviour
{
    //player est l'avatar du joueur
    public GameObject player;


    //volumeManagers correspond au script permettant de gérer le volume
    private VolumeManager volumeManager;

    //ennemisSpawner est le script gérant l'apparition des énnemis
    private SpawnEnnemis ennemisSpawner;

    //healthbar est le script de gestion de la barre de vie du joueur
    private HealthScript healthBar;

    //canvasRectTransform est le transform du canvas principale de la scène
    private RectTransform canvasRectTransform;

    //playerController correspond au script de controle du joueur
    private PlayerController playerController;

    //speed est la vitesse du personnage de transition
    private float speed = 100.0f;

    //transitionAnimator correspond à l'animator du personnage de transition
    private Animator transitionAnimator;

    //audioSource correspond à l'audio source de l'animation
    private AudioSource audioSource;

    //target correspond à la destination du personnage de transition
    private Vector2 target;
    //position correspond à la position actuel du personnage de transition
    private Vector2 position;

    
    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de volumeManager
        while (volumeManager == null)
        {
            volumeManager = GameObject.FindGameObjectWithTag("Parameters").GetComponent<VolumeManager>();
        }

        //initialisation de canvasRectTransform
        while (canvasRectTransform == null)
        {
            canvasRectTransform = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<RectTransform>();
        }
        
        //initialisation de ennemisSpawner
        while(ennemisSpawner == null)
        {
            ennemisSpawner = GameObject.FindGameObjectWithTag("EnnemisSpawner").GetComponent<SpawnEnnemis>();
        }

        //initialisation de healthBar
        while(healthBar == null)
        {
            healthBar = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<HealthScript>();
        }

        //initialisation de playerController
        while(playerController == null)
        {
            playerController = player.GetComponent<PlayerController>();
        }

        //initialisation de transitionAnimator
        while(transitionAnimator == null)
        {
            transitionAnimator = this.GetComponent<Animator>();
        }
        
        //intialisation de audioSource
        while(audioSource == null)
        {
            audioSource = this.GetComponent<AudioSource>();
        }
        //modification du volume du son
        audioSource.volume = volumeManager.GetMusicVolume();
        //initialisation de target au centre de l'écran
        target = new Vector2(canvasRectTransform.GetComponent<RectTransform>().rect.width / 2,
                             canvasRectTransform.GetComponent<RectTransform>().rect.height / 2);

        //initialisation de position
        position = gameObject.transform.position;
    }

    //appeller à chaque tick
    void Update()
    {

        //si le personnage n'a pas atteint target
        if ((Vector2)transform.position != target)
        {
            //création du step permettant au personnage d'avancer
            float step = speed * Time.deltaTime;
            //modification de la position du personnage
            transform.position = Vector2.MoveTowards(transform.position, target, step);
        }
        //sinon si le personnage l'animator n'est pas à jour
        else if(!transitionAnimator.GetBool("isInCenter"))
        {
            //mise à jour de l'animator
            transitionAnimator.SetBool("isInCenter", true);
            //lancement de la musique de changement de personnage
            audioSource.Play();
        }
        
    }

    //fonction permettant de mettre fin à la transition
    private void EndTransition()
    {
        //reset du nombre de points de vie du joueur
        healthBar.ResetLifePoint();
        //relancement du spawn des ennemis
        ennemisSpawner.SpawnBack();
        //réapparition du joueur
        player.SetActive(true);
        //revive du joueur
        playerController.Revive();
    }
}
