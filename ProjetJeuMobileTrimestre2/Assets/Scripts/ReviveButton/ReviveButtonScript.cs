﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReviveButtonScript : MonoBehaviour
{
    //player correspond à l'avatar du joueur
    public GameObject player;

    //transitionGuy correspond au personnage de transition
    private GameObject transitionGuy;
    //objectToHide correspond au objet à cacher
    private GameObject[] objectToHide;

    //est appeller à l'activation de l'objet
    private void Start()
    {
        //initialisation de transitionGuy
        transitionGuy = transform.parent.GetChild(2).gameObject;
        //initialisation de objectToHide
        objectToHide = new GameObject[4];
        objectToHide[0] = transform.parent.GetChild(0).gameObject;
        objectToHide[1] = transform.parent.GetChild(1).gameObject;
        objectToHide[2] = transform.parent.GetChild(4).gameObject;
        objectToHide[3] = player;
        //implémentation de la fonction onClick du bouton
        this.GetComponent<Button>().onClick.AddListener(LaunchReviveTransition);
    }

    //permet de lancer le revive du personnage
    private void LaunchReviveTransition()
    {
        //activation de transitionGuy
        transitionGuy.SetActive(true);
        //pour chaque objet à cacher
        for(int i = 0; i < objectToHide.Length; i++)
        {
            //désactivation de l'objet à cacher
            objectToHide[i].SetActive(false);
        }
        //destruction du bouton revive
        Destroy(this.gameObject);
    }
}
