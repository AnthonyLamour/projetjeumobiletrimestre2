﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DurationBarController : MonoBehaviour
{
    //gameOverPanel correspond au panel de game over
    public GameObject gameOverPanel;


    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;

    //ennemisSpawner correspon au script gérant le spawn des ennemis
    private SpawnEnnemis ennemisSpawner;

    //barColor correspond au différentes couleurs de la barre de durabilité
    private Color32[] barColor;

    //rectTransform correspond au tranform de l'objet
    private RectTransform rectTransform;

    //image correspond à l'image de l'objet
    private Image image;

    //brokenSwordSript correspond au script gérant l'affichage du game over lié à la destruction de l'épée
    private ShowLastChild brokenSwordScript;

    //maxDuration correspond à la durabilité maximale de l'épée du joueur
    private int maxDuration;

    //baseScale correspond au scale de base de la barre
    private Vector2 baseScale;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //initialisaiont de ennemisSpawner
        while (ennemisSpawner == null)
        {
            ennemisSpawner = GameObject.FindGameObjectWithTag("EnnemisSpawner").GetComponent<SpawnEnnemis>();
        }
        
        //initialisation de rectTransform
        while(rectTransform == null)
        {
            rectTransform = this.GetComponent<RectTransform>();
        }

        //initialisation de image
        while(image == null)
        {
            image = this.GetComponent<Image>();
        }

        //initialisaiont de brokenSwordScript
        while(brokenSwordScript == null)
        {
            brokenSwordScript = gameOverPanel.GetComponent<ShowLastChild>();
        }

        //initialisation de maxDuration
        maxDuration = 100;
        //initialisation de baseScale 
        baseScale = rectTransform.localScale;
        //initialisation de barColor avec 3 couleur vert orange rouge
        barColor = new Color32[3];
        barColor[0] = new Color32(59, 255, 00, 255);
        barColor[1] = new Color32(255, 95, 00, 255);
        barColor[2] = new Color32(204, 00, 07, 255);

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //permet de mettre à jour la barre de durabilité
    public void BarUpdate(int currentBarStatus)
    {
        //modificaiton du scale de la barre
        rectTransform.localScale = new Vector2((currentBarStatus * baseScale.x) / maxDuration,
                                                rectTransform.localScale.y);
        //si la barre est vide
        if (rectTransform.localScale.x <= 0)
        {
            rectTransform.localScale = new Vector2(0,rectTransform.localScale.y);
            //activation de gameOverPanel
            gameOverPanel.SetActive(true);
            //activation du game over par destruction d'épée
            brokenSwordScript.BrokenSword();
            //arrêt du spawn des ennemis
            ennemisSpawner.StopSpawning();
        }
        //si la barre est à moins de 30%
        if (currentBarStatus <= (30 * maxDuration) / 100)
        {
            //alors modification de la couleur de la barre en rouge
            image.color = barColor[2];
        }
        //sinon si la barre est à moins de 60%
        else if(currentBarStatus <= (60 * maxDuration) / 100)
        {
            //alors modification de la couleur de la barre en orange
            image.color = barColor[1];
        }
        //sinon
        else
        {
            //modification de la couleur de la barre en vert
            image.color = barColor[0];
        }
    }

    //permet de récupérer maxDuration
    public int GetMaxDuration()
    {
        //renvoi de maxDuration
        return maxDuration;
    }
}
