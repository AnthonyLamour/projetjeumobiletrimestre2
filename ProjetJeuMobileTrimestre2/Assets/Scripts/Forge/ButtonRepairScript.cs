﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonRepairScript : MonoBehaviour
{
    //buttonRectTransform correspond au transform du boutton
    private RectTransform buttonRectTransform;

    //repairController correspond au script gérant la réparation de l'épée
    private RepairController repairController;

    //repairPower correspond à la valeur de réparation du bouton
    private int repairPower;

    //est appeller à l'activation de l'objet
    private void Start()
    {
        //définition de la fonction onClick du bouton
        this.GetComponent<Button>().onClick.AddListener(StartRepair);
    }

    private void OnEnable()
    {
        //initialisation de buttonRectTransform
        while (buttonRectTransform == null)
        {
            buttonRectTransform = this.GetComponent<RectTransform>();
        }

        //initialisation de repairController
        while (repairController == null)
        {
            repairController = this.transform.parent.parent.gameObject.GetComponent<RepairController>();
        }
    }

    //permet de définir une taille de bouton aléatoire ainsi qu'une valeur de réparation
    public void SetSize()
    {
        //choix random de la taille
        int rndSize = Random.Range(0, 3);

        //en fonction de la taille modification du scale du bouton ainsi que de sa valeur de réparation
        switch (rndSize)
        {
            case 0:
                buttonRectTransform.localScale = new Vector2(0.5f, 0.5f);
                repairPower = 15;
                break;
            case 1:
                buttonRectTransform.localScale = new Vector2(0.7f, 0.7f);
                repairPower = 10;
                break;
            case 2:
                buttonRectTransform.localScale = new Vector2(1f, 1f);
                repairPower = 5;
                break;
        }

    }

    //permet de réparer l'épée au click sur le bouton
    private void StartRepair()
    {
        //début de la réparation de l'épée
        repairController.Reparation(repairPower);
        //désactivation du bouton
        this.gameObject.SetActive(false);
    }

}
