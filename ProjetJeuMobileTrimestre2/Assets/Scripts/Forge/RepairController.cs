﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepairController : MonoBehaviour
{

    //repairSFX correspond aux effet sonore de la réparation de l'épée
    public AudioClip[] repairSFX;

    //player correspond à l'avatar du joueur
    private GameObject player;

    //volumeManagers correspond au script permettant de gérer le volume
    private VolumeManager volumeManager;

    //buttonPool correspond au l'object pool contenant les boutons de réparation
    private ButtonPoolScript buttonPool;

    //audioSource correspond à l'audio de réparation
    private AudioSource audioSource;

    //spawnEnnemi correspond au scrpit de spawn des ennemis
    private SpawnEnnemis spawnEnnemi;

    //stop correspond au temps de fin de réparation
    private float stop;
    //stopDelay correspond au temps de réparation
    private float stopDelay;
    //nextSpwan correspond au temps du prochain sapwn de bouton
    private float nextSpawn;
    //spawnDelay correspon au temps de spawn
    private float spawnDelay;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de volumeManager
        while (volumeManager == null)
        {
            volumeManager = GameObject.FindGameObjectWithTag("Parameters").GetComponent<VolumeManager>();
        }

        //initialisation de player
        while (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        //initialisation de audioSource
        while(audioSource == null)
        {
            audioSource = this.GetComponent<AudioSource>();
        }

        //modification du volume du son
        audioSource.volume = volumeManager.GetSFXVolume();
        //initialisation de spawnDelay
        spawnDelay = 0.5f;
    }

    //est appeller à chaque tick
    void Update()
    {
        //si le temps est inférieur au temps de fin de réparation
        if (Time.time < stop)
        {
            //si le temps est supérieur au temps du prochain spawn
            if (Time.time > nextSpawn)
            {
                //modification du temps du prochain spawn
                nextSpawn = Time.time + spawnDelay;
                //spawn d'un bouton
                buttonPool.SpawnButton();
            }
        }
        //sinon
        else
        {
            //réactivation du player
            player.transform.GetChild(0).GetComponent<Image>().enabled=true;
            player.transform.GetChild(1).GetComponent<Image>().enabled=true;
            //désactivation de cet objet
            this.gameObject.SetActive(false);
        }
    }

    //est appeller à chaque activation de l'objet
    private void OnEnable()
    {
        //initialisation de buttonPool
        while(buttonPool == null)
        {
            buttonPool = transform.GetChild(transform.childCount - 1).gameObject.GetComponent<ButtonPoolScript>();
        }

        //initialisation de spawnEnnemi
        while (spawnEnnemi == null)
        {
            spawnEnnemi = GameObject.FindGameObjectWithTag("EnnemisSpawner").GetComponent<SpawnEnnemis>();
        }

        //initialisation de stopDelay
        if(stopDelay != 2)
        {
            stopDelay = 7 - (int)(spawnEnnemi.GetSpeed() / 50);
        }
        //initialisation de stop
        stop = Time.time + stopDelay;
        //initialisation de nextSpawn
        nextSpawn = Time.time;
        //reset de buttonPool
        buttonPool.ResetPool();
    }

    //permet de réparer l'épée du joueur
    public void Reparation(int repairPoint)
    {
        //initialisation d'une variable random
        int rndSound = Random.Range(0, repairSFX.Length);
        //démarrage d'un son de réparation aléatoire
        audioSource.clip = repairSFX[rndSound];
        audioSource.Play();
        //réparation de l'épée du joueur
        player.GetComponent<SwordDuration>().RepairSword(repairPoint);
    }

}
