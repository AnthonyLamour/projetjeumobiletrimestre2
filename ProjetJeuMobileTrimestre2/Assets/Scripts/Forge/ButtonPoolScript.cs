﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPoolScript : MonoBehaviour
{

    //permet de gérer le spawn des boutons de réparation
    public void SpawnButton()
    {
        //initialisation d'un compteur
        int i = 0;
        //initialisation du prochain bouton à spawn
        GameObject tmpButton = null;
        //temps que le compteur ne dépasse pas le nombre d'enfant du spawner et que le prochain bouton n'a pas été trouver
        while(i<this.transform.childCount && tmpButton == null)
        {
            //si le prochain bouton n'est pas actif
            if (!this.transform.GetChild(i).gameObject.activeInHierarchy)
            {
                //modification de tmpButton
                tmpButton = this.transform.GetChild(i).gameObject;
            }
            //incrémentation du compteur
            i = i + 1;
        }
        //si le prochain bouton a été trouver
        if (tmpButton != null)
        {
            //activation du bouton
            tmpButton.SetActive(true);
            //définition de la taille aléatoire du bouton
            tmpButton.GetComponent<ButtonRepairScript>().SetSize();
            //spawn aléatoire du bouton sur l'écran
            float xRandom = Random.Range(50f, this.GetComponent<RectTransform>().rect.width - 50f);
            float yRandom = Random.Range(50f, this.GetComponent<RectTransform>().rect.height - 50f);
            tmpButton.GetComponent<RectTransform>().position = new Vector2(xRandom, yRandom);
        }
    }

    //permet de reset le spawn des boutons de réparation
    public void ResetPool()
    {
        //pour chaque enfant du spawner
        for(int i = 0; i < this.transform.childCount; i++)
        {
            //désactivation de l'enfant
            this.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

}
