﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForgeController : MonoBehaviour
{
    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;
    //repairPanel correspond au panel de réparation de l'épée
    public GameObject repairPanel;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //définition de la fonction onClick du bouton
        this.GetComponent<Button>().onClick.AddListener(Forge);

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //permet d'activer le panel de réparation de l'épée
    private void Forge()
    {
        //activation du panel de réparation de l'épée
        repairPanel.SetActive(true);
    }

}
