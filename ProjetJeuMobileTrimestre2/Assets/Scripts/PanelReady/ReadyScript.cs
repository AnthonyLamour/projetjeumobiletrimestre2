﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadyScript : MonoBehaviour
{
    //ennemiSpawner correspond à l'objet gérant le spawn d'ennemis
    private SpawnEnnemis ennemiSpawner;

    //stop correspond au temps de fin de préparation
    private float stop;
    //stopDelay correspond au temps de préparation
    private float stopDelay;

    // Start is called before the first frame update
    void Start()
    {

        //initialisation de ennemiSpawner
        while(ennemiSpawner == null)
        {
            ennemiSpawner = GameObject.FindGameObjectWithTag("EnnemisSpawner").GetComponent<SpawnEnnemis>();
        }

        //initialisation de stopDelay
        stopDelay = 3;
        //initialisation de stop
        stop = Time.time + stopDelay;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > stop)
        {
            ennemiSpawner.SpawnBack();
            Destroy(this.gameObject);
        }
    }
}
