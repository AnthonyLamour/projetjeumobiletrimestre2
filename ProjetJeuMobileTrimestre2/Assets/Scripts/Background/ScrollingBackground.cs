﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;

    //scrollSpeed est la vitesse de scrolling des backgrounds
    private float scrollSpeed;

    //background1RectTransform corresond au tranform du background1
    private RectTransform background1RectTransform;
    //background2RectTransform corresond au tranform du background2
    private RectTransform background2RectTransform;
    //canvasRectTransform correspond au transform du canvas
    private RectTransform canvasRectTransform;

    //est appeller lors de l'activation de l'objet
    private void Start()
    {
        //initialisation de loadingPanel
        while(loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //initialisation de background1RectTransform
        while(background1RectTransform == null)
        {
            background1RectTransform = transform.GetChild(1).gameObject.GetComponent<RectTransform>();
        }

        //initialisation de background2RectTransform
        while (background2RectTransform == null)
        {
            background2RectTransform = transform.GetChild(2).gameObject.GetComponent<RectTransform>();
        }

        //initialisation de canvasRectTransform
        while (canvasRectTransform == null)
        {
            canvasRectTransform = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<RectTransform>();
        }

        //initialisation de scrollSpeed
        scrollSpeed = 300;

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
        
    }

    //est appeller à chaque tick
    void Update()
    {

        //modification de la position du background1
        background1RectTransform.position = new Vector2(background1RectTransform.position.x,
                                                        background1RectTransform.position.y - Time.deltaTime * scrollSpeed);

        //si le background1 est complètement en dehors de l'écran
        if (background1RectTransform.position.y <= -canvasRectTransform.rect.height / 2)
        {
            //repositionnement de background1 en haut de l'écran
            background1RectTransform.position = new Vector2(background1RectTransform.position.x,
                                                            canvasRectTransform.rect.height * 1.5f);
        }

        //modification de la position du background1
        background2RectTransform.position = new Vector2(background2RectTransform.position.x,
                                                        background2RectTransform.position.y - Time.deltaTime * scrollSpeed);

        //si le background1 est complètement en dehors de l'écran
        if (background2RectTransform.position.y <= -canvasRectTransform.rect.height / 2)
        {
            //repositionnement de background1 en haut de l'écran
            background2RectTransform.position = new Vector2(background2RectTransform.position.x,
                                                            canvasRectTransform.rect.height * 1.5f);
        }
        
    }
}
