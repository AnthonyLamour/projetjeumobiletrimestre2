﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class EnnemisScript : MonoBehaviour
{

    //dyingSounds correspond à la liste des songs de morts des ennemis
    public PlayableAsset[] dyingSounds;

    //volumeManagers correspond au script permettant de gérer le volume
    private VolumeManager volumeManager;

    //ennemiRigibody2D correspond au rigidbody de l'ennemi
    private Rigidbody2D ennemiRigidbody2D;

    //ennemiAnimator correspond à l'animator de l'ennemi
    private Animator ennemiAnimator;

    //playableDirector correspond au playble director de l'ennemi
    private PlayableDirector playableDirector;

    //addScore correspond au script permettant d'ajouter du score au joueur
    private AddScore addScore;

    //ennemiSprite correspond au sprite de l'ennemi
    private Image ennemiSprite;

    //shadow correspond à l'ombre de l'ennemi
    private Image shadow;

    //speed correspond à la vitesse de déplacement des ennemis
    private float speed;
    
    //isMoving correspond à l'état de l'ennemis
    private bool isMoving;

    //damage correspond au dommage que l'ennemis inflige à l'épée 
    private int damage;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de volumeManager
        while (volumeManager == null)
        {
            volumeManager = GameObject.FindGameObjectWithTag("Parameters").GetComponent<VolumeManager>();
        }

        //initialisation de ennemiRigidbody2D
        while (ennemiRigidbody2D == null)
        {
            ennemiRigidbody2D = this.GetComponent<Rigidbody2D>();
        }

        //initialisation de ennemiSprite
        while(ennemiSprite == null)
        {
            ennemiSprite = this.transform.GetChild(1).gameObject.GetComponent<Image>();
        }

        //initialisation de ennemiSprite
        while (shadow == null)
        {
            shadow = this.transform.GetChild(0).gameObject.GetComponent<Image>();
        }

        //initialisation de ennemiAnimator
        while(ennemiAnimator == null)
        {
            ennemiAnimator = this.transform.GetChild(1).gameObject.GetComponent<Animator>();
        }

        //initialisation de playableDirector
        while(playableDirector == null)
        {
            playableDirector = this.GetComponent<PlayableDirector>();
        }

        //initialisation de addScore
        while(addScore == null)
        {
            addScore = transform.parent.gameObject.GetComponent<AddScore>();
        }

        //modification du volume
        this.GetComponent<AudioSource>().volume = volumeManager.GetMusicVolume();

        //modification du volume
        this.GetComponent<AudioSource>().volume = volumeManager.GetSFXVolume();

        //initialisaiton de speed
        speed = 0;
        //initialisation de isMoving
        isMoving = false;
        //initialisaiton de damage
        damage = 10;
    }

    //est appeller à chaque tick
    void Update()
    {
        //si l'ennemis est en mouvement
        if (isMoving)
        {
            //modification de la vélocité de l'ennemis
            ennemiRigidbody2D.velocity = new Vector2(0, speed);
        }
    }

    //permet de changer le statue de l'ennemis
    public void SwitchIsMoving()
    {
        //si l'ennemis est en mouvement
        if (isMoving)
        {
            //modification de isMoving
            isMoving = false;
            //reset speed
            speed = 0;
            //reset de la vélocité de l'ennemis
            ennemiRigidbody2D.velocity = new Vector2(0, 0);
        }
        //sinon
        else
        {
            //modificaiton de isMoving
            isMoving = true;
        }
    }

    //permet de setter la vitesse de l'ennemis
    public void SetSpeed(float newSpeed)
    {
        //modificaiton de la vitesse de l'ennemis
        speed = newSpeed;
    }

    //permet de reset l'ennemis
    public void ResetEnnemi()
    {
        //reset de la position de l'ennemsi en dehors de l'écran
        this.transform.position = new Vector2(-2500, 2500);
        //désactivation des sprites de l'ennemi
        ennemiSprite.enabled = false;
        shadow.enabled = false;
        //appelle de SwitchMoving
        SwitchIsMoving();
        //mise à jour de l'animator de l'ennemi
        ennemiAnimator.SetBool("isDying", false);
    }

    //en cas de collision de l'objet
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //si l'objet entrant en collision est le joueur
        if (collision.gameObject.tag == "Player")
        {
            //initialisation d'un entier random pour le son de mort
            int rnd = Random.Range(0, dyingSounds.Length);
            //démarrage du son de mort
            playableDirector.Play(dyingSounds[rnd]);
            //endommagemant de l'épée du joueur
            collision.GetComponent<SwordDuration>().DamageSword(damage);
            //ajout de score
            addScore.AddPlayerScore(50);
            //mise à jour de l'animator de l'ennemi
            ennemiAnimator.SetBool("isDying", true);
        }
    }
}
