﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DyingScript : MonoBehaviour
{

    //permet de déclancher la mort de l'ennemis
    private void StartDying()
    {
        //appelle de la mort de l'ennemis
        this.transform.parent.gameObject.GetComponent<EnnemisScript>().ResetEnnemi();
    }
}
