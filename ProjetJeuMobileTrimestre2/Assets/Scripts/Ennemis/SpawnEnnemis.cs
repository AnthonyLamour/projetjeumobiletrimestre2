﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnEnnemis : MonoBehaviour
{
    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;
    //divineSword correspond au bonus de l'épée divine
    private GameObject divineSword;
    //spwaner correspond à l'objet pool des ennemis
    private GameObject spawner;

    //canvasRectTranform correspond au RectTransform canvas principale de la scène
    private RectTransform canvasRectTransform;

    //divineSwordController correspond au script de la divineSword
    private DivineSwordController divineSwordController;

    //spawnDelay correspond au délais entre chaque spawn
    private float spawnDelay;
    //nextSpawnTime correspond au prochain temps de spawn
    private float nextSpawnTime;
    //speed correspond à la vitesse de déplacement des ennemis
    private float speed;
    
    //isSpawning correspond à l'état du spawner
    private bool isSpawning;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //initialisation de spawner
        while (spawner == null)
        {
            spawner = GameObject.FindGameObjectWithTag("EnnemisPool");
        }
        
        //initialisation de divineSword
        while(divineSword == null)
        {
            divineSword = GameObject.FindGameObjectWithTag("DivineSword");
        }
        
        //initialisation de canvasRectTransform
        while (canvasRectTransform == null)
        {
            canvasRectTransform = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<RectTransform>();
        }

        //intitialisation de divineSwordController
        while(divineSwordController == null)
        {
            divineSwordController = divineSword.GetComponent<DivineSwordController>();
        }

        //initialisation de spawnDelay
        spawnDelay = 2;
        //initialisation de nextSpawnTime
        nextSpawnTime = Time.time + spawnDelay;
        //initialisation de speed
        speed = 100;
        //initialisation de isSpawning
        isSpawning = false;

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //est appeller à chaque tick
    void Update()
    {
        //si le temps du prochain spawn est dépasser et si le spawner est actif
        if (Time.time > nextSpawnTime && isSpawning)
        {
            //initialisation d'un compteur
            int i = 0;
            //initialisaiton d'un objet correspondant au prochain ennemis à spawn
            GameObject nextEnnemisToSpawn=null;
            //initilialisation d'une variable random
            int rndForge = Random.Range(0, 11);
            //si la variable random atteint la bonne valeur et que le bonus de l'épée divine n'est pas en mouvement
            if (rndForge == 5 && !divineSwordController.GetIsMoving())
            {
                //positionnement aléatoire sur l'axe X de l'épée
                float posX = Random.Range(100f, canvasRectTransform.rect.width - 100f);
                //positionnement de l'épée
                divineSword.transform.position = new Vector2(posX, canvasRectTransform.rect.height);
                //modification de l'état de l'épée
                divineSwordController.SwitchIsMoving();
                //set de la vitesse de l'épée
                divineSwordController.SetSpeed(-200f);
                //set du prochain temps de spawn
                nextSpawnTime = Time.time + spawnDelay;
            }
            //temps que le compteur ne dépasse pas le nombre d'enfant du spawner et que le prochain ennemis n'a pas été trouver
            while (i<spawner.transform.childCount && nextEnnemisToSpawn==null)
            {
                //si le prochain enfant du spawner n'est pas actif
                if (spawner.transform.GetChild(i).transform.GetChild(0).GetComponent<Image>().enabled == false)
                {
                    //modification de nextEnnemisToSpawn
                    nextEnnemisToSpawn = spawner.transform.GetChild(i).gameObject;
                }
                //incrémentation du compteur
                i = i + 1;
            }
            //si le prochain ennemis a été défini
            if (nextEnnemisToSpawn != null)
            {
                //activation de l'ennemi
                nextEnnemisToSpawn.transform.GetChild(0).GetComponent<Image>().enabled = true;
                nextEnnemisToSpawn.transform.GetChild(1).GetComponent<Image>().enabled = true;
                //positionnement aléatoire sur l'axe X de l'ennemi
                float posX = Random.Range(100f, canvasRectTransform.rect.width - 100f);
                //positionnemnet de l'ennemi
                nextEnnemisToSpawn.transform.position = new Vector2(posX, canvasRectTransform.rect.height);
                //modification de l'état de l'ennemi
                nextEnnemisToSpawn.GetComponent<EnnemisScript>().SwitchIsMoving();
                //set de la vitesse de l'ennemi
                nextEnnemisToSpawn.GetComponent<EnnemisScript>().SetSpeed(-speed);
                //set du prochain temps de spawn
                nextSpawnTime = Time.time + spawnDelay;
                
            }
        }
    }

    //permet d'arrêter le spawn des ennemis et de nettoyer l'écran de tout ennemis
    public void StopSpawning()
    {
        //modification de l'état du spawner
        isSpawning = false;
        //récupération de tout les ennemis
        GameObject[] ennemis = GameObject.FindGameObjectsWithTag("Ennemi");
        //pour chaque ennemis
        for (int i = 0; i < ennemis.Length; i++)
        {
            //si l'ennemis est actif
            if (ennemis[i].transform.GetChild(0).GetComponent<Image>().enabled)
            {
                //reset de l'ennemi
                ennemis[i].GetComponent<EnnemisScript>().ResetEnnemi();
            }
        }
        //si le bonus d'épée divine est actif
        if (divineSwordController.GetIsMoving())
        {
            //reset du bonus d'épée divine
            divineSwordController.ResetDivineSword();
        }
    }

    //permet de reprendre le spawn d'ennemis
    public void SpawnBack()
    {
        //modification de l'état du spawner
        isSpawning = true;
    }

    //permet d'augmenter la vitesse des ennemis
    public void IncreaseSpeed(float speedToAdd)
    {
        if(speed==250 && spawnDelay > 1)
        {
            spawnDelay = spawnDelay - 0.2f;
        }
        else
        {
            //modification de la vitesse
            speed = speed + speedToAdd;
        }
    }

    //permet de récupérer la vitesse des ennemis
    public float GetSpeed()
    {
        return speed;
    }
}
