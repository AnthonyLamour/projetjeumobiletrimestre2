﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameZoneScript : MonoBehaviour
{

    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;

    //healthBar correspond au script de barre de vie du joueur
    private HealthScript healthBar;

    //est appeller à l'activation de l'objet
    private void Start()
    {

        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //initialisation de healthBar
        while (healthBar == null)
        {
            healthBar = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<HealthScript>();
        }

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //en cas de sortie de collision
    private void OnTriggerExit2D(Collider2D collision)
    {
        //si l'objet sortant de la zone est un ennemi
        if (collision.tag == "Ennemi")
        {
            //si l'ennemi est actif
            if (collision.transform.GetChild(0).GetComponent<Image>().enabled == true)
            {
                //reset de l'ennemi
                collision.GetComponent<EnnemisScript>().ResetEnnemi();
                //update de la barre de vie du joueur
                healthBar.LoseLifePoint();
            }
            
        }
    }
}
