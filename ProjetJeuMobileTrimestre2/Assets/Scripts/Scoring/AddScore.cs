﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScore : MonoBehaviour
{

    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;
    
    //playerScore correspond au texte du score
    private ScoreController playerScore;


    //est appeller à l'activation de l'objet
    private void Start()
    {
        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }
        
        //initialisation de scoreController
        while(playerScore == null)
        {
            playerScore = GameObject.FindGameObjectWithTag("TextScore").GetComponent<ScoreController>();
        }

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //permet d'ajouter du score
    public void AddPlayerScore(int scoreValue)
    {
        //ajout de score
        playerScore.AddScore(scoreValue);
    }
}
