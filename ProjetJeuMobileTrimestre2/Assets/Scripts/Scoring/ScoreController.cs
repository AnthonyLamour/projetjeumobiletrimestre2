﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreController : MonoBehaviour
{
    //spawnScript correspond au script gérant le spawn des ennemis
    public SpawnEnnemis spawnScript;

    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;

    //scoreChecker correspond à une variable permettant de vérifier le score
    private int scoreChecker;
    
    //scoreText correspond au textMeshPro du score
    private TextMeshProUGUI scoreText;

    //est appeller à l'activation de l'objet
    private void Start()
    {
        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //initialisation de scoreText
        while (scoreText == null)
        {
            scoreText = this.GetComponent<TextMeshProUGUI>();
        }

        //initialisation de scoreChecker
        scoreChecker = 0;

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //permet d'ajouter du score
    public void AddScore(int scoreToAdd)
    {
        //incrémentation du scoreChecker
        scoreChecker = scoreChecker + 1;
        //récupération du score actuel
        int currentScore = int.Parse(scoreText.text);
        //ajout du score
        currentScore = currentScore + scoreToAdd;
        //modification du texte de score
        scoreText.text = currentScore.ToString();
        //si le joueur à tué 10 ennemis en plus
        if(scoreChecker%10 == 0)
        {
            //augementation de la vitesse du jeu
            spawnScript.IncreaseSpeed(25f);
        }
    }

}
