﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicVolumeLoader : MonoBehaviour
{
    //volumeManagers correspond au script permettant de gérer le volume
    private VolumeManager volumeManager;

    //est appeller à l'activation de l'objet 
    void Start()
    {
        //initialisation de volumeManager
        while (volumeManager == null)
        {
            volumeManager = GameObject.FindGameObjectWithTag("Parameters").GetComponent<VolumeManager>();
        }

        //modification du volume
        this.GetComponent<AudioSource>().volume = volumeManager.GetMusicVolume();
    }

}
