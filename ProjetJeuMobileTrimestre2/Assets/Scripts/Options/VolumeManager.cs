﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeManager : MonoBehaviour
{
    //soundTest correspond au script permettant de faire les sound test
    private SoundTest soundTest;

    //musicSource correspond à l'audio source de la musique
    private AudioSource musicSource;

    //musicVolume correspond au volume de la musique
    private float musicVolume;
    //sfxVolume correspond au volume des effets sonores
    private float sfxVolume;

    //est appeller à l'activation de l'objet 
    private void Start()
    {
        //permet de faire en sorte que l'objet ne soit pas détruit
        DontDestroyOnLoad(this.gameObject);
        //initialisation de musicVolume
        musicVolume = 0.5f;
        //initialisation de sfxVolume
        sfxVolume = 0.5f;
    }

    //permet de changer le volume de la musique
    public void SetMusicVolume(float newMusicVolume)
    {
        //modification du volume de la musique
        musicVolume = newMusicVolume;
        //initialisation de musicSource
        while (musicSource == null)
        {
            musicSource = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>();
        }
        //application de la modification du volume de la musique
        musicSource.volume = musicVolume;
    }

    //permet de changer le volume des effets sonores
    public void SetSFXVolume(float newSFXVolume)
    {
        //modification du volume des effets sonores
        sfxVolume = newSFXVolume;
        //initialisation de soundTest
        while (soundTest == null)
        {
            soundTest = GameObject.FindGameObjectWithTag("SoundTest").GetComponent<SoundTest>();
        }
        //lancement du sound test
        soundTest.TestSoundSFX(sfxVolume);
    }

    //permet a d'autre objet de récupérer le volume de la music
    public float GetMusicVolume()
    {
        //renvoi de musicVolume
        return musicVolume;
    }

    //permet a d'autre objet de récupérer le volume des effets sonores
    public float GetSFXVolume()
    {
        //renvoi de sfxVolume
        return sfxVolume;
    }
}
