﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicVolumeChange : MonoBehaviour
{

    //volumeManagers correspond au script permettant de gérer le volume
    private VolumeManager volumeManager;

    //musicSlider correspond au slider permettant de régler le volume de la musique
    private Slider musicSlider;

    //est appeller à l'activation de l'objet 
    void Start()
    {
        //initialisation de volumeManager
        while(volumeManager == null)
        {
            volumeManager = GameObject.FindGameObjectWithTag("Parameters").GetComponent<VolumeManager>();
        }

        //initialisation de musicSlider
        while(musicSlider == null)
        {
            musicSlider = this.GetComponent<Slider>();
        }

    }

    //permet d'initier le changement de volume de la musique
    public void ChangeMusicVolume()
    {
        //changement de volume de la musique
        volumeManager.SetMusicVolume(musicSlider.value/10);
    }

}
