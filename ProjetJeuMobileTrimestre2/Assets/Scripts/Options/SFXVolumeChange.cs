﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SFXVolumeChange : MonoBehaviour
{
    //volumeManagers correspond au script permettant de gérer le volume
    private VolumeManager volumeManager;

    //sfxSlider correspond au slider permettant de régler le volume de la musique
    private Slider sfxSlider;

    //est appeller à l'activation de l'objet 
    void Start()
    {
        //initialisation de volumeManager
        while (volumeManager == null)
        {
            volumeManager = GameObject.FindGameObjectWithTag("Parameters").GetComponent<VolumeManager>();
        }

        //initialisation de sfxSlider
        while (sfxSlider == null)
        {
            sfxSlider = this.GetComponent<Slider>();
        }

    }

    //permet d'initier le changement de volume des sfx
    public void ChangeSFXVolume()
    {
        //changement de volume des sfx
        volumeManager.SetSFXVolume(sfxSlider.value / 10);
    }
}
