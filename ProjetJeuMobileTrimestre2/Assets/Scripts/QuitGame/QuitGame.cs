﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    //permet de quitter le jeu
    public void QuitTheGame()
    {
        //fermeture de l'application
        Application.Quit();
    }
}
