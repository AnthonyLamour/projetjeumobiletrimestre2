﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAttaking : MonoBehaviour
{
    //permet d'arrêter l'attaque du joueur
    public void StopAttakingFunction()
    {
        //mise à jour de l'animator
        this.GetComponent<Animator>().SetBool("IsAttaking", false);
    }

}
