﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordDuration : MonoBehaviour
{
    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;

    //durationBarController correspond au script gérant la barre de durabilité de l'épée
    private DurationBarController durationBarController;

    //swordDuration correspond à la durabilité actuel de l'épée
    private int swordDuration;
    //maxSwordDuration correspond à la durabilité maximale de l'épée
    private int maxSwordDuration;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //intialisation de durationBarController
        while(durationBarController == null)
        {
            durationBarController = GameObject.FindGameObjectWithTag("SwordBar").GetComponent<DurationBarController>();
        }

        //initialisation de swordDuration
        swordDuration = durationBarController.GetMaxDuration();
        //initialisation de maxSwordDuration
        maxSwordDuration = durationBarController.GetMaxDuration();

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //permet d'endomager l'épée
    public void DamageSword(int damagePoint)
    {
        //modification de swordDuration
        swordDuration = swordDuration - damagePoint;
        //mise à jour de la barre de durabilité de l'épée
        durationBarController.BarUpdate(swordDuration);
        //si la durabilité de l'épée est inférieur est inférieur ou égale à 0
        if (swordDuration <= 0)
        {
            //désactivation de l'objet
            this.gameObject.SetActive(false);
        }
    }

    //permet de réparer l'épée
    public void RepairSword(int repairPoint)
    {
        //si la réparation de l'épée dépasser la durabilité maximale de l'épée
        if (swordDuration + repairPoint > maxSwordDuration)
        {
            //modification de la durabilité de l'épée
            swordDuration = maxSwordDuration;
        }
        //sinon
        else
        {
            //modification de la durabilité de l'épée
            swordDuration = swordDuration + repairPoint;
        }
        //mise à jour de la barre de durabilité de l'épée
        durationBarController.BarUpdate(swordDuration);
    }

    //permet de reset la durabilité de l'épée
    public void ResetSwordDuration()
    {
        //modification de la durabilité de l'épée
        swordDuration = maxSwordDuration;
        //mise à jour de la barre de durabilité de l'épée
        durationBarController.BarUpdate(swordDuration);
    }

}
