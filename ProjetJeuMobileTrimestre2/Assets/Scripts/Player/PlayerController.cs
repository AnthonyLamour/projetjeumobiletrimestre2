﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour, IDragHandler, IBeginDragHandler
{

    //swordSounds correspond au différent bruitage de l'épée du joueur
    public AudioClip[] swordSounds;

    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;
    //shadow correspond à l'ombre du joueur
    private GameObject shadow;

    //volumeManagers correspond au script permettant de gérer le volume
    private VolumeManager volumeManager;

    //playerRectTransform correspond au transform du joueur
    private RectTransform playerRectTransform;
    //canvasRectTransform correspond au transform du canvas principale de la scène
    private RectTransform canvasRectTransform;

    //audioSource correspond à l'audio source du joueur
    private AudioSource audioSource;

    //playerAnimator correspond à l'animator du joueur
    private Animator playerAnimator;

    //lastMousePosition correspond à la dernière position du curseur ou du doigt
    private Vector2 lastMousePosition;
    //baseSize correspond à la taille de base du personnage
    private Vector2 baseSize;
    //currentMousePosition correspond à la position actuel du curseur ou du doigt
    private Vector2 currentMousePosition;
    //diff correspond à la différence entre la position du joueur et du curseur ou doigt
    private Vector2 diff;

    //newPosition correspond à la nouvelle position du joueur
    private Vector3 newPosition;
    //oldPos correspond à l'ancienne position du joueur
    private Vector3 oldPos;

    //isAlive correspond au statue du joueur
    private bool isAlive;
    

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de volumeManager
        while (volumeManager == null)
        {
            volumeManager = GameObject.FindGameObjectWithTag("Parameters").GetComponent<VolumeManager>();
        }

        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //initialisation de canvasRectTransform
        while (canvasRectTransform == null)
        {
            canvasRectTransform = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<RectTransform>();
        }

        //initialisation de playerAnimator
        while(playerAnimator == null)
        {
            playerAnimator = this.transform.GetChild(1).gameObject.GetComponent<Animator>();
        }

        //initialisation de playerRectTransform
        while (playerRectTransform == null)
        {
            playerRectTransform = this.GetComponent<RectTransform>();
        }
        
        //initialisation de audioSource
        while(audioSource == null)
        {
            audioSource = this.GetComponent<AudioSource>();
        }

        //intialisation de shadow
        while(shadow == null)
        {
            shadow = this.transform.GetChild(0).gameObject;
        }

        //modification du volume du son
        audioSource.volume = volumeManager.GetSFXVolume();
        //initialisation de isAlive
        isAlive = true;
        //initialisation de baseSize
        baseSize = playerRectTransform.sizeDelta;
        //initialisation de currentMousePosition
        currentMousePosition = new Vector2(0, 0);
        //initialisation de diff
        diff = new Vector2(0, 0);
        //initialisation de newPosition
        newPosition = new Vector3(0, 0, 0);
        //initialisation de oldPos
        oldPos = new Vector3(0, 0, 0);

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }

    //est appeller en cas de collision avec un autre objet
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //si le joueur est vivant
        if (isAlive)
        {
            //si l'objet toucher est un ennemi
            if (collision.tag == "Ennemi")
            {
                //mise à jour de l'animator
                playerAnimator.SetBool("IsAttaking", true);
                //initialisation d'une variable random
                int rnd = Random.Range(0, swordSounds.Length);
                //démarrage d'un son d'épée aléatoire
                audioSource.clip = swordSounds[rnd];
                audioSource.Play();
            }
        }
    }

    //est appeller au début du drag
    public void OnBeginDrag(PointerEventData eventData)
    {
        //si le joueur est vivant
        if (isAlive)
        {
            //modification de lastMousePosition
            lastMousePosition = eventData.position;
        }
    }

    //est appeller pendant le drag
    public void OnDrag(PointerEventData eventData)
    {
        //si le joueur est vivant
        if (isAlive)
       {
            //enregistrement de la position actuelle du curseur ou du doigt
            currentMousePosition = eventData.position;
            //si cette position est en dehors de l'écran alors repositionnement de la position
            if (currentMousePosition.x < 100)
            {
                currentMousePosition.x = 100;
            }
            if (currentMousePosition.x > canvasRectTransform.rect.width - 100)
            {
                currentMousePosition.x = canvasRectTransform.rect.width - 100;
            }
            if (currentMousePosition.y < 0)
            {
                currentMousePosition.y = 0;
            }
            if (currentMousePosition.y > canvasRectTransform.rect.height - ((canvasRectTransform.rect.height * 20) / 100) - 50)
            {
                currentMousePosition.y = canvasRectTransform.rect.height - ((canvasRectTransform.rect.height * 20) / 100) - 50;
            }

            //calcule de la différence entre la position du joueur et du curseur ou doigt
            diff = currentMousePosition - lastMousePosition;
            //définition de la nouvelle position
            newPosition = playerRectTransform.position + new Vector3(diff.x, diff.y, transform.position.z);
            //définition de l'ancienne position
            oldPos = playerRectTransform.position;
            //modification de la position du joueur
            playerRectTransform.position = newPosition;
            //enregistrement de la dernière position du curseur ou du doigt
            lastMousePosition = currentMousePosition;
        }
       
    }

    //permet de gérer la mort du joueur
    public void Dying()
    {
        //mise à jour de l'animator
        playerAnimator.SetBool("isDying", true);
        //désactivation de l'ombre du joueur
        shadow.SetActive(false);
        //resize du joueur
        playerRectTransform.sizeDelta = new Vector2(baseSize.x * 2, baseSize.y * 2);
        //positionnement du joueur au centre de l'écran
        playerRectTransform.position = new Vector2(canvasRectTransform.rect.width / 2,
                                                   canvasRectTransform.rect.height / 2);
        //modification du statue du joueur
        isAlive = false;
    }

    //permet de gérer le revive du joueur
    public void Revive()
    {
        //mise à jour de l'animator
        playerAnimator.SetBool("isRevive", true);
        playerAnimator.SetBool("isDying", false);
        //désactivation de la transition et du panel de game over
        GameObject.FindGameObjectWithTag("TransitionGuy").SetActive(false);
        GameObject.FindGameObjectWithTag("GameOverPanel").SetActive(false);
        //resize du joueur
        playerRectTransform.sizeDelta = baseSize;
        //reset de la durabilité de l'épée du joueur
        this.GetComponent<SwordDuration>().ResetSwordDuration();
        shadow.SetActive(true);
        isAlive = true;
    }
}
