﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour
{
    //dyingPanel correspond au panel de game over
    public GameObject dyingPanel;


    //loadingPanel correspond au panel de chargement
    private GameObject loadingPanel;

    //playerController correspond au controller du joueur
    private PlayerController playerController;

    //ennemisSpwaner correspond au script gérant le spawn des ennemis
    private SpawnEnnemis ennemisSpawner;

    //maxLifePoint correspond au nombre de points de vies maximale du joueur
    private int maxLifePoint;
    //currentLifePoint correspond au nombre de points de vies actuel du joueur
    private int currentLifePoint;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de loadingPanel
        while (loadingPanel == null)
        {
            loadingPanel = GameObject.FindGameObjectWithTag("LoadingPanel");
        }

        //initialisation de player
        while(playerController == null)
        {
            playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        }
        
        //initialisation de ennemisSpawner
        while(ennemisSpawner == null)
        {
            ennemisSpawner = GameObject.FindGameObjectWithTag("EnnemisSpawner").GetComponent<SpawnEnnemis>();
        }
        
        //initialisation de maxLifePoint
        maxLifePoint = 3;
        //initialisation de currentLifePoint
        currentLifePoint = 3;

        //notification de fin de chargement de l'objet
        loadingPanel.GetComponent<LoadingScript>().Notify();
    }
    
    //permet de faire perdre des points de vie au joueur
    public void LoseLifePoint()
    {
        //si les points de vie restant sont suprérieur à 0
        if (currentLifePoint > 0)
        {
            //désactivation d'un coeur
            this.transform.GetChild(currentLifePoint-1).GetComponent<Image>().enabled = false;
        }
        //diminution du nombre de points de vie restant
        currentLifePoint = currentLifePoint - 1;
        //si le nombre de points de vie restant est 0
        if (currentLifePoint == 0)
        {
            //declanchement de la mort du player
            playerController.Dying();
            //activation du panel de game over
            dyingPanel.SetActive(true);
            //arrêt du spawn des ennemis
            ennemisSpawner.StopSpawning();
        }
    }

    //permet de reset les points de vie du joueur
    public void ResetLifePoint()
    {
        //reset des points de vie du joueur
        currentLifePoint = maxLifePoint;
        //réaffichage des coeurs du joueur
        for(int i = 0; i < transform.childCount; i++)
        {
            this.transform.GetChild(i).GetComponent<Image>().enabled = true;
        }
    }
}
