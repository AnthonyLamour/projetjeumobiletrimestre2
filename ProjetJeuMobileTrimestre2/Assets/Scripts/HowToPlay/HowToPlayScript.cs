﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayScript : MonoBehaviour
{
    //panels correspond à la liste des panels de tutoriel
    private GameObject[] panels;
    //currentPanel correspond au panel actuellemet affiché
    private GameObject currentPanel;

    //currentPanelPosition correspond au numéro du panel actuellement affiché
    private int currentPanelPosition;

    //est appeller à l'activation de l'objet
    void Start()
    {
        //initialisation de panels
        while(panels == null)
        {
            panels = new GameObject[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
            {
                panels[i] = transform.GetChild(i).gameObject;
            }
        }
        
        //intialisation de currentPanel
        while(currentPanel == null)
        {
            currentPanel = panels[0];
        }
        
        //initialisation de currentPanelPosition
        currentPanelPosition = 0;
    }

    //fonction permettant de montrer le panel suivant
    public void ShowNextPanel()
    {
        //si on ne dépasse pas le nombre de panel max
        if(currentPanelPosition + 1 < transform.childCount)
        {
            //désactivation du panel en cours
            currentPanel.SetActive(false);
            //incrémentation de currentPanelPosition
            currentPanelPosition++;
            //modification de currentPanel
            currentPanel = transform.GetChild(currentPanelPosition).gameObject;
            //activation du nouveaux panel
            currentPanel.SetActive(true);
        }
    }

    //fonction permettant de montrer le panel précédent
    public void ShowPreviousPanel()
    {
        //si on ne tombe pas en dessous de 0
        if (currentPanelPosition - 1 >= 0)
        {
            //désactivation du panel en cours
            currentPanel.SetActive(false);
            //décrémentation de currentPanelPosition
            currentPanelPosition--;
            //modification de currentPanel
            currentPanel = transform.GetChild(currentPanelPosition).gameObject;
            //activation du nouveaux panel
            currentPanel.SetActive(true);
        }
    }
}
